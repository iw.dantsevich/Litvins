# Litvins

## Getting Started

Clone the project into your working directory:

```git clone *github url*```

Install external dependencies:

``` yarn install ```

``` cd frontend and yarn install```

Run API server: ```npm run server```

Run static server: ```cd frontend and npm start```

### Available addresses:

http://localhost:4000/ - Node.js server

http://localhost:3000/ - React project



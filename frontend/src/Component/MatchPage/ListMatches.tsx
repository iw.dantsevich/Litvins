import React, {useEffect, useState} from 'react';
import HeaderContainer from "../Header/HeaderContainer";
import styles from './ListMatch.module.css'
import AsideContainer from "../Aside/AsideContainer";
import InfoMatch from "./InfoMatch";
import {getMatches, getNextMatch} from "../../action/matchsActions";
import {useDispatch, useSelector} from "react-redux";
import {StateType, MatchType} from '../../common/types'

const ListMatches = () => {
  const dispatch = useDispatch()
  const listMatches = useSelector((state: StateType) => state.matchesPage.matches)
  const nextMatch = useSelector((state: StateType) => state.matchesPage.nextMatch)
  const [playedMatch, setPlayedMatch] = useState({});

  useEffect(() => {
    dispatch(getMatches())
    dispatch(getNextMatch())
  }, [dispatch])
  useEffect(() => {
    checkMatches()
  }, [listMatches])

  const checkMatches = (): any => {
    const currentD = new Date();
    listMatches.forEach((match) => {
      const checkDate = new Date(match.dateTime)
      if (currentD > checkDate && !match.score) {
        setPlayedMatch(match)
      }
    })
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <HeaderContainer
          nextMatch={nextMatch}
          title={'MATCH LIST'}
          activeLink={'Список матчей'}
          header={true}
          link={''}
          childrenLink={''}
        />
      </div>
      <div className={styles.container}>
        <div className={styles.container__content}>
          <AsideContainer/>
          <div className={styles.content}>
            <div className={styles.listMatch}>
              {
                listMatches.map((match: MatchType) => {
                  return <InfoMatch match={match} key={match._id}/>
                })
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ListMatches

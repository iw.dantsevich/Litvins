import React, {FC, useEffect} from 'react';
import HeaderContainer from "../Header/HeaderContainer";
import styles from './NewsPage.module.css'
import NewsContent from "./NewsContent";
import PaginatorNews from "../innerComponent/Paginator";
import AsideContainer from "../Aside/AsideContainer";
import {useDispatch, useSelector} from "react-redux";
import {getListNews} from "../../action/newsActions";
import {NewsInfoType, NewsPageType, StateType} from '../../common/types'

export type customStylesProps = {
  content: {
    // width: string,
    // height: string,
    // top: string,
    // left: string,
    // right: string,
    // bottom: string,
    // marginRight: string,
    // transform: string,
    // zIndex: number,
    // position: string,
    // opacity: number
  }
}

export const customStyles: customStylesProps = {
  content: {
    width: '700px',
    height: '300px',
    top: '55%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    zIndex: 9999,
    position: 'relative',
    opacity: 1
  }
};


const NewsPage: FC<NewsPageType> = () => {
  const dispatch = useDispatch()
  const listNews = useSelector((state: StateType) => state.newsPage.newsList)
  const currentPage = useSelector((state: StateType) => state.newsPage.currentPage)
  const pageSize = useSelector((state: StateType) => state.newsPage.pageSize)
  const totalPlayersCount = useSelector((state: StateType) => state.newsPage.totalPlayersCount)
  const message = useSelector((state: StateType) => state.newsPage.message)

  useEffect(() => {
    const getAllNews = () => {
      dispatch(getListNews(1, 9))
    }
    getAllNews()
  }, [dispatch])

  const dispatchMethod = (pageNumber: number) => {
    dispatch(getListNews(pageNumber, pageSize))
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <HeaderContainer title={'Новости'} activeLink={'Новости'}/>
      </div>
      <div className={styles.wrapper__container}>
        <div className={styles.wrapper__container__wrap}>
          <AsideContainer/>
          <div className={styles.wrapper__container__wrap__info}>
            {!!message ? <div>{message}</div> : null}

            <div className={styles.wrapper__container__content}>
              {listNews.map((news: NewsInfoType) => {
                return <NewsContent news={news} key={news._id}/>
              })}
            </div>
            <div className={styles.paginator}>
              <PaginatorNews
                currentPage={currentPage}
                pageSize={pageSize}
                totalPlayersCount={totalPlayersCount}
                onDispatchMethod={dispatchMethod}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default NewsPage

import {matchesApi} from "../api/api";
import {setLastMatch, setMatch, setMatches, setMessage, setNextMatch} from "../reducers/MatchsReducer";

export const getMatches = () => async (dispatch: any) => {
    const data = await matchesApi.getMatches()
    dispatch(setMatches(data))

}
export const getMatch = (matchId: any) => async (dispatch: any) => {
    const data = await matchesApi.getMatch(matchId)
    dispatch(setMatch(data))
}

export const getNextMatch = () => async (dispatch: any) => {
    const data = await matchesApi.getNextMatch()
    if (data === null) {
        dispatch(setMessage('Будующих мачей нету'))
    } else {
        dispatch(setNextMatch(data))
    }
}
export const getLastMatch = () => async (dispatch: any) => {
    const data = await matchesApi.getLastMatch()
    dispatch(setLastMatch(data))
}

// : any 5


// не соответствие передаваемых параметров  в файле AddResultMatch.tsx только 1 параметр передавали  dispatch(addResultMatch(formData))
// export const addResultMatch = (result: any, matchID: any) => async (dispatch: any) => {
//     const data = await matchesApi.addResultMatch(result, matchID)
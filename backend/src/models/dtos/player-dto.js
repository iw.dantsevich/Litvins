// module.exports = class PlayerDto {
// 	id;
// 	email;
// 	role;
// 	isEmailConfirmed;
// 	constructor(model) {
// 		this.id = model._id;
// 		this.email = model.email;
// 		this.role = model.role;
// 		this.isEmailConfirmed = model.isEmailConfirmed;
// 	}
// };

module.exports = class PlayerDto {
	constructor(model) {
		delete model._doc.password;
		delete model._doc.emailConfirmGuid;
		delete model._doc.resetPasswordGuid;
		for (var key in model._doc) {
			this[key] = model[key];
		}
	}
};

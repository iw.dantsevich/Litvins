process.env['NODE_CONFIG_DIR'] = __dirname + '/config';
const express = require('express');
const config = require('config');
const path = require('path');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const connectDB = require('./db');
const multer = require('multer');
const { urlencoded } = require('body-parser');
const MongoStore = require('connect-mongo');
const app = express();
const PORT = process.env.PORT || 4000;
const ENV = process.env.NODE_ENV || 'Development';
const db = config.get('TEST_DB_URL');

app.disable('x-powered-by');
app.use(cookieParser());
app.use(express.json());
app.use(urlencoded({ extended: true }));

//Настройки для хранилища сессий (https://www.npmjs.com/package/connect-mongo)
app.use(
	session({
		secret: '$ecrett',
		resave: false,
		saveUninitialized: false,
		rolling: true,
		store: MongoStore.create({ mongoUrl: db }),
	})
);

require('./routers')(app);

if (process.env.NOdE_ENV === 'production') {
	app.use('/', express.static(path.join(__dirname, 'client', 'build')));
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
	});
}
module.exports.start = async function start() {
	try {
		await connectDB();
		app.listen(PORT, () =>
			console.log(`server is listing on ${PORT} - ${ENV} environment`)
		);
	} catch (e) {
		console.log('Server Error', e.message);
		process.exit(1);
	}
};

module.exports.app = app;

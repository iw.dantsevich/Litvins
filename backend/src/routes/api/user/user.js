const Player = require('../../../models/Player');
const bcrypt = require('bcrypt');
const { Router } = require('express');
const router = Router();
const {
	sendConfirmEmailMail,
	sendResetPasswordMail,
} = require('../../../services/mailService.js');
const { checkEmailConfirm } = require('../../../services/middleware.js');
const { object, string, number, date } = require('yup');
const PlayerDto = require('../../../models/dtos/player-dto');
const uuid = require('uuid');
const { startSession } = require('mongoose');

router.post('/login', login);
router.get('/logout', logout);
router.get('/reset', resetPassword);
router.post('/reset', resetPassword);
router.get('/confirmEmail', confirmEmail);
router.post('/register', registration);
router.get('/status', status);

const registerShema = object({
	email: string().email('Uncorrect email'),
	password: string()
		.required()
		.matches(
			/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/,
			'Password must have minimum eight characters, at least one uppercase letter, one lowercase letter and one number'
		),
	numberPlayer: number('numberPlayer must be a number')
		.min(1, 'Number player must be more than 0')
		.max(99, 'Number player must be less than 100')
		.integer('integer only'),
	birthday: date('invalid date'),
});

async function registration(req, res) {
	try {
		const playerData = req.body.data;
		const markup = req.body.markup;
		const subject = req.body.subject;

		const { email, password, birthday, numberPlayer } = playerData;
		await registerShema.validate(
			{
				email,
				password,
				birthday,
				numberPlayer,
			},
			{ abortEarly: false }
		);
		const found = await Player.exists({ email });
		if (found) {
			return res.status(400).json({
				message: `User ${email} allready exist...`,
				success: false,
			});
		}
		const hashedPassword = await bcrypt.hash(password, 8);
		const guid = uuid.v4();

		const session = await startSession();
		session.startTransaction();
		console.log(session.inTransaction());
		const player = await Player.create(
			[
				{
					...playerData,
					password: hashedPassword,
					emailConfirmGuid: guid,
				},
			],
			{ session }
		);

		const sendEmailResult = await sendConfirmEmailMail(
			email,
			guid,
			markup ??
				`<h3>Что бы подтвердить email, перейдите по <a href="@href@">ссылке.</a></h3>`,
			subject ?? 'Подтвердите Ваш email.'
		);
		if (!sendEmailResult) {
			session.endSession();
			return res.status(500).json({
				message:
					'Internal server error (mailService not work... try later.)',
			});
		}
		await session.commitTransaction();
		session.endSession();
		res.status(201).json({
			message: 'User created',
			success: true,
			user: new PlayerDto(player[0]),
		});
	} catch (err) {
		res.status(500).json({
			message: err.message,
			errors: err.errors,
			success: false,
		});
	}
}

async function confirmEmail(req, res) {
	try {
		const { guid } = req.query;
		const player = await Player.findOne({ emailConfirmGuid: guid });
		if (!player) {
			return res.status(400).json({
				message: `Bad guid `,
				success: false,
			});
		}
		if (player.isEmailConfirmed) {
			return res.status(200).json({
				message: `Email ${player.email} allready confirmed... `,
				success: true,
			});
		}
		await player.update({ isEmailConfirmed: true, emailConfirmGuid: '' });
		return res.json({
			message: `Email ${player.email} has been confirmed!`,
			success: true,
		});
	} catch (err) {
		res.status(500).json({
			message: err.message,
			success: false,
		});
	}
}

async function login(req, res) {
	try {
		const { email, password } = req.body.data;
		const player = await Player.findOne({ email });
		if (!player) {
			return res
				.status(400)
				.json({ message: 'User not found', success: false });
		}
		const isMatch = await bcrypt.compare(password, player.password);
		if (!isMatch) {
			return res.status(400).json({
				message: 'Uncorrect login and/or password, try again',
				success: false,
			});
		}
		req.session.user = player;
		res.json({
			message: 'Login success.',
			user: new PlayerDto(player),
		});
	} catch (e) {
		res.status(500).json({
			message: e.message,
			success: false,
		});
	}
}

async function status(req, res) {
	try {
		const { user } = req.session;
		if (user) {
			return res.json(new PlayerDto(user));
		}
		res.json({ message: 'You are not authorized' });
	} catch (error) {
		res.json(error.message);
	}
}

async function logout(req, res) {
	try {
		await req.session.destroy();
		res.status(200).json({ message: 'Logout success', success: true });
	} catch (err) {
		res.status(500).json({
			message: err.message,
			success: false,
		});
	}
}

async function resetPassword(req, res) {
	try {
		if (req.method === 'GET') {
			const { email, markup, subject } = req.query;
			if (!email) {
				return res.status(400).json({
					message: 'Bad or empty email in query',
					success: false,
				});
			}
			const player = await Player.findOne({ email });
			if (!player) {
				return res.status(400).json({
					message: `User with email ${email} not found`,
					success: false,
				});
			}
			const guid = uuid.v4();
			await player.update({ resetPasswordGuid: guid });
			const sendEmailResult = await sendResetPasswordMail(
				email,
				guid,
				markup,
				subject
			);
			if (!sendEmailResult) {
				return res.status(500).json({
					message:
						'Internal server error (mailService not work... try later.)',
				});
			}
			res.status(200).json({
				message: `Link to set new password has been sended to your email (${email})`,
				success: true,
			});
		}
		if (req.method === 'POST') {
			const { guid, password } = req.body.data;
			const player = await Player.findOne({
				resetPasswordGuid: guid,
			});
			if (!player || guid === '') {
				return res.status(400).json({
					message: `Bad guid`,
					success: false,
				});
			}
			await registerShema.validate({ password });
			const hashedPassword = await bcrypt.hash(password, 8);
			await player.update({
				password: hashedPassword,
				resetPasswordGuid: '',
			});
			return res.status(200).json({
				message: 'Password changed',
				success: true,
			});
		}
	} catch (err) {
		res.status(400).json({
			message: err.message,
			errors: err.errors,
			success: false,
		});
	}
}

module.exports = router;

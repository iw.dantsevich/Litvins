const { createTransport } = require('nodemailer');
const config = require('config');
const smtpName = require('config').get('SMTP_LOGIN');
const smtpPass = require('config').get('SMTP_PASSWORD');

const transporter = createTransport({
	service: 'gmail',
	auth: {
		user: smtpName,
		pass: smtpPass,
	},
});

async function sendMail(params) {
	console.log('sendEmail: ', params);
	try {
		const result = await transporter.sendMail(params);
		return result;
	} catch (err) {
		throw new Error('Some server error...');
	}
}

async function sendConfirmEmailMail(email, guid, markup, subject) {
	const href = `${config.get('SERVER_URL')}/user/reset?guid=${guid}`;
	const html = markup.replace('@href@', href);
	const options = {
		from: smtpName,
		to: email,
		subject,
		text: '',
		html,
	};
	return await sendMail(options);
}

async function sendResetPasswordMail(email, guid, markup, subject) {
	const href = `${config.get('SERVER_URL')}/user/reset?guid=${guid}`;
	const html = markup.replace('@href@', href);
	const options = {
		from: smtpName,
		to: email,
		subject,
		text: '',
		html,
	};
	return await sendMail(options);
}

module.exports = { sendConfirmEmailMail, sendResetPasswordMail };

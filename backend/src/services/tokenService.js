const jwt = require('jsonwebtoken');

const tokenService = {
	// generateAccessToken(payload) {
	// 	return jwt.sign(payload, config.get('SECRET_KEY'), {
	// 		expiresIn: config.get('ACCESS_TOKEN_EXPIRIES_IN'),
	// 	});
	// },

	generateConfirmToken(payload) {
		return jwt.sign(payload, 'SECRET_KEY');
	},

	decodeToken(token) {
		try {
			return jwt.verify(token, 'SECRET_KEY');
		} catch (error) {
			throw new Error('Unable to verify token: ' + error.message);
		}
	},
};

module.exports = { tokenService };

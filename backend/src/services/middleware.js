const paginate = require('jw-paginate');
const moment = require('moment');

function getPaginator(req, arr) {
	const currentPage = parseInt(req.query.page) || 1;
	const pageSize = parseInt(req.query.count) || 8;
	const pager = paginate(arr.length, currentPage, pageSize);
	const pageOfItems = arr.slice(pager.startIndex, pager.endIndex + 1);
	return { pager, pageOfItems };
}

function combineDateAndTime(date, time) {
	let dataValue = moment(date).format('ll');
	let timeValue = moment(time).format('HH:mm:ss');
	const dateTime = new Date(`${dataValue} ${timeValue}`);
	return moment(dateTime).format('lll');
}

// middleware отклоняет запросы от пользователя, не подтвердившего свой email
function checkEmailConfirm(req, res, next) {
	if (req.session.user && !req.session.user.isEmailConfirmed) {
		return res.status(401).json({
			message: 'You are not confirm your email',
			success: false,
		});
	}
	next();
}

function injectAuthData(req, res, next) {
	req.user = req.session.user;
	next();
}

module.exports = {
	getPaginator,
	combineDateAndTime,
	injectAuthData,
	checkEmailConfirm,
};
